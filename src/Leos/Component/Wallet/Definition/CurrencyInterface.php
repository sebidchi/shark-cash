<?php

namespace Leos\Component\Wallet\Definition;

/**
 * Class CurrencyInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Definition
 */
interface CurrencyInterface
{
    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return float
     */
    public function getExchangeRate(): float;
}