<?php

namespace Leos\Component\Wallet\Definition;

/**
 * Class WalletInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Definition
 */
interface WalletInterface
{
    /**
     * WalletInterface constructor.
     *
     * @param int $realCredit
     * @param int $bonusCredit
     */
    public function __construct(int $realCredit  = 0, int $bonusCredit = 0);

    /**
     * @return int
     */
    public function getRealCredit(): int;

    /**
     * @param int $realCredit
     * @return WalletInterface
     */
    public function setRealCredit(int $realCredit): WalletInterface;

    /**
     * @return int
     */
    public function getBonusCredit(): int;

    /**
     * @param int $bonusCredit
     * @return WalletInterface
     */
    public function setBonusCredit(int $bonusCredit): WalletInterface;

    /**
     * @param CreditInterface $credit
     * @return WalletInterface
     */
    public function addRealCredit(CreditInterface $credit): WalletInterface;

    /**
     * @param CreditInterface $credit
     * @return WalletInterface
     */
    public function addBonusCredit(CreditInterface $credit): WalletInterface;

    /**
     * @param CreditInterface $credit
     * @return WalletInterface
     */
    public function removeRealCredit(CreditInterface $credit): WalletInterface;

    /**
     * @param CreditInterface $credit
     * @return WalletInterface
     */
    public function removeBonusCredit(CreditInterface $credit): WalletInterface;

}
