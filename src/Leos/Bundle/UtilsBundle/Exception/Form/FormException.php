<?php
namespace Leos\Bundle\UtilsBundle\Exception\Form;

use Symfony\Component\Form\FormInterface;

/**
 * Class FormException
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UtilsBundle\Exception\Form
 */
class FormException extends \Exception
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @param FormInterface $form
     * @return FormException
     */
    public function setForm(FormInterface $form): FormException
    {
        $this->form = $form;

        return $this;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }
}