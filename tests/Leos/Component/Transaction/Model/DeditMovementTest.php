<?php
namespace Tests\Leos\Component\Transaction\Model;

use Leos\Component\Transaction\Model\DebitMovement;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class DebitMovementTest
 * 
 * @package Tests\Leos\Component\Transaction\Model
 */
class DebitMovementTest extends WebTestCase
{

    public function testDebit()
    {
        $debit = new DebitMovement(TransactionCategoryTest::getTransactionCategory("bet"));

        $this->assertNotNull($debit->getCategory());
        // Real
        $this->assertEquals(0, $debit->setReal(0.00)->getReal()->getAmount());
        $this->assertEquals(1000, $debit->setReal(10.00)->getReal()->getAmount());
        $this->assertEquals(1050, $debit->setReal(10.50)->getReal()->getAmount());
        // Bonus
        $this->assertEquals(0, $debit->setBonus(0.00)->getBonus()->getAmount());
        $this->assertEquals(1000, $debit->setBonus(10.00)->getBonus()->getAmount());
        $this->assertEquals(1050, $debit->setBonus(10.50)->getBonus()->getAmount());
    }
}
