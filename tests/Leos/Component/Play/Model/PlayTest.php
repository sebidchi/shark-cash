<?php
namespace Tests\Leos\Component\Play\Model;

use Leos\Component\Play\Model\Play;
use Leos\Component\Transaction\Model\CreditMovement;
use Leos\Component\Transaction\Definition\TransactionInterface;

use Tests\Leos\Component\Transaction\Model\TransactionCategoryTest;
use Tests\Leos\Component\Transaction\Model\TransactionTest;

/**
 * Class PlayTest
 *
 * @package Tests\Leos\Component\Play\Model
 */
class PlayTest extends \PHPUnit_Framework_TestCase
{

    const
        TEST_ROUND_ID = "qwer1ty2ui3",
        TEST_SESSION_ID = "1k23h1k31h2kj4b12",
        TEST_GAME_OK_ID = [
            1,
            "2",
            1234567890
        ],
        TEST_GAME_KO_ID = [
            null,
            "two"
        ],
        TYPES = [
            'BET',
            'WIN',
            'ROLLBACK_BET',
            'ROLLBACK_WIN'
        ]
    ;

    /**
     * @dataProvider examplesOK
     *
     * @param int $game
     * @param string $session
     * @param string $round
     * @param string $type
     */
    public function testPlay(int $game, string $session, string $round, string $type)
    {
        $play = $this->getPlay($game, $session, $round, $type);

        $this->assertEquals(self::TEST_SESSION_ID, $play->getSession());
        $this->assertEquals($type, $play->getTransaction()->getCategory()->getName());
        $this->assertTrue($play->getTransaction() instanceof TransactionInterface);

        $this->assertNotNull($play->getSession());
        $this->assertNotNull($play->getRound());

        $this->assertNotNull($play->getB2bReal());
        $this->assertNotNull($play->getB2bBonus());
    }

    /**
     * @dataProvider examplesKO
     * @expectedException \TypeError
     *
     * @param int $game
     * @param string $session
     * @param string $round
     */
    public function testPlayKO(int $game, string $session, string $round)
    {
        $this->expectException($this->getPlay($game, $session, $round));
    }

    /**
     * @return array
     */
    public function examplesOK()
    {
        return [
            [self::TEST_GAME_OK_ID[0], self::TEST_SESSION_ID, self::TEST_ROUND_ID, self::TYPES[0]],
            [self::TEST_GAME_OK_ID[1], self::TEST_SESSION_ID, self::TEST_ROUND_ID, self::TYPES[1]],
            [self::TEST_GAME_OK_ID[2], self::TEST_SESSION_ID, self::TEST_ROUND_ID, self::TYPES[2]],
        ];
    }

    /**
     * @return array
     */
    public function examplesKO()
    {
        return [
            [self::TEST_GAME_KO_ID[0], self::TEST_SESSION_ID, self::TEST_ROUND_ID, self::TYPES[0]],
            [self::TEST_GAME_KO_ID[1], self::TEST_SESSION_ID, self::TEST_ROUND_ID, self::TYPES[1]],
        ];
    }

    /**
     * @param int $gameId
     * @param string $session
     * @param string $round
     * @param string $type
     * @return \Leos\Component\Play\Definition\PlayInterface
     */
    public static function getPlay(int $gameId = 0, string $session = "", string $round = "", string $type)
    {
        return (new Play($gameId, $session, $round))
            ->setTransaction(
                TransactionTest::getTransaction(
                    new CreditMovement(
                        TransactionCategoryTest::getTransactionCategory($type),
                        50.00, 25.00
                    )
                )
        );
    }

}
