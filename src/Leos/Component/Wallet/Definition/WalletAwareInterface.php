<?php
namespace Leos\Component\Wallet\Definition;

/**
 * Class WalletAwareInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Definition
 */
interface WalletAwareInterface
{

    /**
     * @return WalletInterface
     */
    public function getWallet(): WalletInterface;
}