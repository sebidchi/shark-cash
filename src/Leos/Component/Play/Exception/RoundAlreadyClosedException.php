<?php

namespace Leos\Component\Play\Exception;

/**
 * Class RoundAlreadyClosedException
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Exception
 */
class RoundAlreadyClosedException extends \LogicException
{

    /**
     * RoundAlreadyClosedException constructor.
     */
    public function __construct()
    {
        parent::__construct('round.exception.round_already_closed', 409);
    }
}