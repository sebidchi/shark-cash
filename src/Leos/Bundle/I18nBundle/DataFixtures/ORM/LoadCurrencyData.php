<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 01/05/16
 * Time: 11:27
 */

namespace Leos\Bundle\I18nBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Leos\Bundle\I18nBundle\Entity\Currency;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadCurrencyData
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\I18nBundle\DataFixtures\ORM
 */
class LoadCurrencyData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->container->getParameter('leos.fixtures.currency') as $data) {

            $currency = new Currency($data['code'], $data['exchange']);

            $manager->persist($currency);
            $manager->flush();

            $this->setReference('currency-' . $currency->getCode(), $currency);
        }
    }

    public function getOrder()
    {
        return 1;
    }

}