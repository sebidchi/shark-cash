<?php

namespace Leos\Component\Transaction\Model;

use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Wallet\Definition\CreditInterface;
use Leos\Component\Wallet\Model\Credit;

/**
 * Class AbstractMovement
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Model
 */
class AbstractMovement implements MovementInterface
{
    /**
     * @var float
     */
    protected $real = 0.00;

    /**
     * @var float
     */
    protected $bonus = 0.00;

    /**
     * @var TransactionCategory
     */
    protected $category;

    /**
     * AbstractMovement constructor.
     * @param TransactionCategory $category
     * @param float $real
     * @param float $bonus
     */
    public function __construct(TransactionCategory $category, float $real = 0.00, float $bonus = 0.00)
    {
        $this->category = $category;
        $this->real = new Credit($real);
        $this->bonus = new Credit($bonus);
    }

    /**
     * @return CreditInterface
     */
    public function getReal(): CreditInterface
    {
        return $this->real;
    }

    /**
     * @return CreditInterface
     */
    public function getBonus(): CreditInterface
    {
        return $this->bonus;
    }

    /**
     * @param float $amount
     * @return MovementInterface
     */
    public function setReal(float $amount): MovementInterface
    {
        $this->real = new Credit($amount);

        return $this;
    }

    /**
     * @param float $amount
     * @return MovementInterface
     */
    public function setBonus(float $amount): MovementInterface
    {
        $this->bonus = new Credit($amount);

        return $this;
    }

    /**
     * @return TransactionCategory
     */
    public function getCategory(): TransactionCategory
    {
        return $this->category;
    }

    /**
     * @param TransactionCategory $transactionCategory
     * @return MovementInterface
     */
    public function setCategory(TransactionCategory $transactionCategory): MovementInterface
    {
        $this->category = $transactionCategory;

        return $this;
    }

}