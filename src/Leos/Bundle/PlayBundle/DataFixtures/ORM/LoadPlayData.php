<?php
namespace Leos\Bundle\PlayBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Leos\Component\User\Definition\UserInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadPlayData
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\DataFixtures
 */
class LoadPlayData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
{
    $this->container = $container;
}

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->container->getParameter('leos.fixtures.play') as $play) {

            /** @var UserInterface $user */
            $user = $this->getReference('user-' . $play['user']);

            $this->container->get('leos.manager.play')->create(
                $user,
                $play['category'],
                $play['real'],
                $play['bonus'],
                [
                    'session' => $play['session'],
                    'round' => $play['round']
                ]
            );
        }

    }

    public function getOrder()
    {
        return 4;
    }


}