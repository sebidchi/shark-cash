<?php

namespace Leos\Component\User\Definition;
use Leos\Component\Wallet\Definition\CurrencyInterface;

/**
 * Interface UserInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 *
 * @package Leos\Component\User\Definition
 */
interface UserInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @param string $username
     * @return UserInterface
     */
    public function setUsername($username): UserInterface;

    /**
     * @return string
     */
    public function getSlug(): string;

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @param string $password
     * @return UserInterface
     */
    public function setPassword(string $password): UserInterface;

    /**
     * @return string
     */
    public function getSalt(): string;

    /**
     * @return array
     */
    public function getRoles(): array;

    /**
     * @param array $roles
     * @return UserInterface
     */
    public function setRoles(array $roles): UserInterface;

    /**
     * @return CurrencyInterface
     */
    public function getCurrency();

    /**
     * @param CurrencyInterface $currency
     * @return UserInterface
     */
    public function setCurrency($currency): UserInterface;
}

