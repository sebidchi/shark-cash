<?php
namespace Tests\Leos\Component\Play\Model;

use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Model\PlaySessionRound;
use Leos\Component\Transaction\Model\TransactionCategory;

/**
 * Class PlaySessionRoundTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Model
 */
class PlaySessionRoundTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @group unit
     */
    public function testRoundBet()
    {
        $round = $this->getPlaySessionRound(PlaySessionTest::getPlaySession(PlayTest::TEST_SESSION_ID));

        $round->addTransaction(
            PlayTest::getPlay(PlayTest::TEST_GAME_OK_ID[0], PlayTest::TEST_SESSION_ID, PlayTest::TEST_ROUND_ID, "bet")
        );

        $this->assertNotNull($round->getSession());
        $this->assertNotNull($round->getRoundId());

        $this->assertNotNull($round->getUser());

        $this->assertNotNull($round->getBetReal());
        $this->assertNotNull($round->getBetBonus());

        $this->assertNotNull($round->getWinReal());
        $this->assertNotNull($round->getWinBonus());
    }

    /**
     * @group unit
     */
    public function testRoundWin()
    {
        $round = $this->getPlaySessionRound(PlayTest::TEST_SESSION_ID);

        $round->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::BET)
        );

        $this->assertFalse($round->isClosed());

        $win = PlayTest::getPlay(
            PlayTest::TEST_GAME_OK_ID[0],
            PlayTest::TEST_SESSION_ID,
            PlayTest::TEST_ROUND_ID,
            TransactionCategory::WIN
        );

        $win->setClose(true);

        $round->addTransaction(
            $win
        );
        $this->assertNotNull($round->getSession());
        $this->assertNotNull($round->getRoundId());
    }

    /**
     * @group unit
     *
     * @expectedException \TypeError
     */
    public function testRoundIdException()
    {
        $round = $this->getPlaySessionRound();

        $this->expectException($round->getId());
    }

    /**
     * @param PlaySession|null $session
     * @return PlaySessionRound
     */
    public static function getPlaySessionRound($session = null, string $round = null): PlaySessionRound
    {
        $round = new PlaySessionRound($round);

        if ($session) {

            $round->setSession(
                is_string($session) ? PlaySessionTest::getPlaySession($session) : $session
            );
        }

        return $round;
    }
}