<?php
namespace Leos\Component\Play\Factory;

use Leos\Component\Play\Model\Play;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;
use Leos\Component\Transaction\Definition\MovementInterface;

/**
 * Class PlayManagerInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Manager
 */
interface PlayFactoryInterface
{
    /**
     * @param UserInterface $user
     * @param string $category
     * @param float $real
     * @param float $bonus
     * @param array $data
     * @return TransactionAwareInterface
     */
    public function process(UserInterface $user, string $category, float $real, float $bonus, array $data): TransactionAwareInterface;
}
