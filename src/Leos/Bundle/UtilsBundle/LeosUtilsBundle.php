<?php

namespace Leos\Bundle\UtilsBundle;

use Leos\Bundle\UtilsBundle\DependencyInjection\CompilerPass\FOSViewResponseListenerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LeosUtilsBundle
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UtilsBundle
 */
class LeosUtilsBundle extends Bundle
{

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new FOSViewResponseListenerCompilerPass());
    }
}
