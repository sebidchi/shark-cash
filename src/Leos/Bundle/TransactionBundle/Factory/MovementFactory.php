<?php
namespace Leos\Bundle\TransactionBundle\Factory;

use Leos\Component\Transaction\Model\CreditMovement;
use Leos\Component\Transaction\Model\DebitMovement;
use Leos\Component\Transaction\Model\TransactionCategory;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Factory\MovementFactoryInterface;
use Leos\Bundle\TransactionBundle\Repository\TransactionCategoryRepository;

/**
 * Class MovementFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\TransactionBundle\Facroty
 */
class MovementFactory implements MovementFactoryInterface
{
    /**
     * @var TransactionCategoryRepository
     */
    private $categoryRepository;

    /**
     * MovementFactory constructor.
     * @param TransactionCategoryRepository $categoryRepository
     */
    public function __construct(TransactionCategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param string $category
     * @param float $real
     * @param float $bonus
     *
     * @return CreditMovement|DebitMovement|MovementInterface
     */
    public function create(string $category, float $real = 0.00, float $bonus = 0.00): MovementInterface
    {
        $transactionCategory = $this->categoryRepository->findOneByName($category);

        if (!$transactionCategory) {

            throw new \LogicException('transaction_category.not_found');
        }

        if ($transactionCategory->getType() == TransactionCategory::CREDIT) {

            return $this->credit($transactionCategory, $real, $bonus);
        }

        if ($transactionCategory->getType() == TransactionCategory::DEBIT) {

            return $this->debit($transactionCategory, $real, $bonus);
        }

        throw new \LogicException("movement.factory.exception.impossible_resolve_type");
    }

    /**
     * @param TransactionCategory $category
     * @param float $real
     * @param float $bonus
     * @return DebitMovement
     */
    private function debit(TransactionCategory $category, float $real, float $bonus): DebitMovement
    {
        return new DebitMovement($category, $real, $bonus);
    }

    /**
     * @param TransactionCategory $category
     * @param float $real
     * @param float $bonus
     * @return CreditMovement
     */
    private function credit(TransactionCategory $category, float $real, float $bonus): CreditMovement
    {
        return new CreditMovement($category, $real, $bonus);
    }

}