<?php
namespace Test\Leos\Component\Transaction\EventListener;

use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Definition\TransactionInterface;
use Leos\Component\Transaction\EventListener\TransactionFactoryEventListener;
use Leos\Component\Transaction\Model\CreditMovement;

use Leos\Component\Transaction\Model\DebitMovement;
use Leos\Component\Transaction\Model\TransactionCategory;
use Tests\Leos\Component\Transaction\Model\TransactionCategoryTest;
use Tests\Leos\Component\User\Model\UserTest;
use Tests\Leos\Component\Transaction\Fixture\Entity\TransactionAwareTest;
use Tests\Leos\Component\Transaction\Fixture\Event\TransactionFactoryEventTest;

/**
 * Class TransactionFactoryEventListenerTest
 *
 * @package Leos\Component\Transaction\EventListener
 */
class TransactionFactoryEventListenerTest extends \PHPUnit_Framework_TestCase
{

    public function testEventListener()
    {
        $this->generateTransaction(
            new CreditMovement(
                TransactionCategoryTest::getTransactionCategory("bet"),
                50.00, 25.00)
        );
    }

    /**
     * @expectedException \LogicException
     */
    public function testNotEnoughFounds()
    {
        $this->generateTransaction(
            new DebitMovement(
                TransactionCategoryTest::getTransactionCategory("win"),
                50.00, 25.00)
        );
    }

    /**
     * @param MovementInterface $movement
     * @throws \Exception
     */
    protected function generateTransaction(MovementInterface $movement)
    {
        (new TransactionFactoryEventListener())
            ->process(
                $originEvent = new TransactionFactoryEventTest(
                    UserTest::getUser(),
                    $movement,
                    new TransactionAwareTest())
            )
        ;

        $this->assertTrue($originEvent->getTransactionAware()->getTransaction() instanceof TransactionInterface);
        $this->assertEquals(5000, $originEvent->getTransactionAware()->getTransaction()->getAmountReal());
        $this->assertEquals(2500, $originEvent->getTransactionAware()->getTransaction()->getAmountBonus());
    }
}
