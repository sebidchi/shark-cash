<?php

namespace Tests\Leos\Component\Transaction\Fixture\Event;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;
use Leos\Component\Transaction\Event\TransactionFactoryEventInterface;

/**
 * Class TransactionFactoryEventTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Tests\Leos\Component\Transaction\Fixture\Event
 */
final class TransactionFactoryEventTest implements TransactionFactoryEventInterface
{
    /**
     * @var TransactionAwareInterface
     */
    private $transactionAware;

    /**
     * @var MovementInterface
     */
    private $movement;

    /**
     * @var WalletAwareInterface|UserInterface
     */
    private $user;

    /**
     * TransactionFactoryEventTest constructor.
     * @param UserInterface $user
     * @param MovementInterface $movementInterface
     * @param TransactionAwareInterface $transactionAwareInterface
     */
    public function __construct(UserInterface $user, MovementInterface $movementInterface, TransactionAwareInterface $transactionAwareInterface)
    {
        $this->transactionAware = $transactionAwareInterface;
        $this->movement = $movementInterface;
        $this->user = $user;
    }

    /**
     * @return TransactionAwareInterface
     */
    public function getTransactionAware(): TransactionAwareInterface
    {
        return $this->transactionAware;
    }

    /**
     * @param TransactionAwareInterface $transactionAware
     * @return TransactionFactoryEventInterface
     */
    public function setTransactionAware(TransactionAwareInterface $transactionAware): TransactionFactoryEventInterface
    {
        $this->transactionAware = $transactionAware;
        return $this;
    }

    /**
     * @return MovementInterface
     */
    public function getMovement(): MovementInterface
    {
        return $this->movement;
    }

    /**
     * @param MovementInterface $movement
     * @return TransactionFactoryEventInterface
     */
    public function setMovement($movement): TransactionFactoryEventInterface
    {
        $this->movement = $movement;
        return $this;
    }

    /**
     * @return WalletAwareInterface|UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param WalletAwareInterface|UserInterface $user
     * @return TransactionFactoryEventInterface
     */
    public function setUser(WalletAwareInterface $user): TransactionFactoryEventInterface
    {
        $this->user = $user;

        return $this;
    }

}
