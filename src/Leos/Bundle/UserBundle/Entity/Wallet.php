<?php

namespace Leos\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use JMS\Serializer\Annotation as Serializer;

use Leos\Component\Wallet\Model\Wallet as BaseWallet;
use Leos\Component\Wallet\Definition\CurrencyInterface;

/**
 * Class Wallet
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UserBundle\Entity
 *
 * @Embeddable()
 * @Serializer\ExclusionPolicy("all")
 */
class Wallet extends BaseWallet
{
    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $realCredit = 0;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $bonusCredit = 0;

}
