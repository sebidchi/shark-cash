<?php

namespace Leos\Bundle\UtilsBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

use Leos\Bundle\UtilsBundle\EventListener\FOSViewResponseListener;

/**
 * Class FOSViewResponseListener
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UtilsBundle\DependencyInjection\CompilerPass
 */
class FOSViewResponseListenerCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('fos_rest.view_response_listener');

        if ($definition){

            $definition->setClass(FOSViewResponseListener::class);
        }
    }
}
