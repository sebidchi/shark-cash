<?php
namespace Leos\Component\I18n\Definition;

/**
 * Class TranslatableInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\I18n\Definition
 */
interface TranslatableInterface
{

    /**
     * @return string
     */
    public function getKey(): string;
}