<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\Controller;
use Leos\Bundle\TransactionBundle\Entity\Transaction;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TransactionController
 *
 * @package AppBundle\Controller\Api
 * @RouteResource("Transaction")
 */
class TransactionController extends Controller
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     description="returns all transactions",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Default", "Basic", "List", "Embed"})
     *
     * @Rest\QueryParam(name="page", default="1", description="The page to display")
     * @Rest\QueryParam(name="limit", default="10", description="The number of items per pager")
     *
     * @param Request $request
     *
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function cgetAction(Request $request)
    {
        return $this->get('leos.utils.pagination')->paginate(
            $this->get('leos.manager.transaction')->all(),
            'get_transactions',
            [
                'version' => $request->get('version'),
                '_format' => $request->getRequestFormat()
            ],
            $request->get('limit', 10),
            $request->get('page', 1)
        );
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Given and id returns the transaction",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Leos\Bundle\TransactionBundle\Entity\Transaction"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Basic", "Embed"})
     *
     * @param int $id
     * @return Transaction
     * @throws \Exception
     */
    public function getAction(int $id): Transaction
    {
        return $this->get('leos.manager.transaction')->findOneByIdOrFail(
            $id,
            new NotFoundHttpException("Transaction not found")
        );
    }
}
