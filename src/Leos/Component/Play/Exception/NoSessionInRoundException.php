<?php

namespace Leos\Component\Play\Exception;

/**
 * Class NoSessionInRoundException
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Exception
 */
class NoSessionInRoundException extends \LogicException
{

    /**
     * NoSessionInRoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('round.exception.session_must_be_initialized', 409);
    }
}