<?php
namespace Leos\Bundle\PlayBundle\Search\Transformer;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Leos\Bundle\PlayBundle\Entity\Play;

use Elastica\Document;
use FOS\ElasticaBundle\Transformer\ModelToElasticaTransformerInterface;

/**
 * Class PlayTransformer
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Search\Transformer
 */
class PlayTransformer implements ModelToElasticaTransformerInterface
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * PlayTransformer constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Play $play
     * @param array $fields
     * @return Document
     */
    public function transform($play, array $fields)
    {
        $object = $this->serializer->toArray($play, (new SerializationContext())->setGroups("Search"));

        $object = array_merge($object, $object['transaction']);

        $object['user'] = $play->getTransaction()->getUser()->getSlug();
        $object['category'] = $play->getTransaction()->getCategory()->getName();
        $object['currency'] = $play->getTransaction()->getUser()->getCurrency()->getCode();

        unset($object['transaction']);

        return new Document(
            $play->getId(),
            $object
        );
    }

}