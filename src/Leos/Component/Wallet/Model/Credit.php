<?php

namespace Leos\Component\Wallet\Model;

use Leos\Component\Wallet\Definition\CreditInterface;

/**
 * Class Credit
 *
 * Credit is the internal casino money used to process money calculations.
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Model
 */
class Credit implements CreditInterface
{

    /**
     * @var int
     */
    protected $amount;

    /**
     * Credit constructor.
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        $this->amount = intval($amount * 100);
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
       return $this->amount;
    }

}
