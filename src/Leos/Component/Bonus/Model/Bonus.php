<?php
namespace Leos\Component\Bonus\Model;

use Leos\Component\Bonus\Definition\BonusInterface;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Wallet\Definition\CurrencyInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;

/**
 * Class Bonus
 *
 * @package Leos\Component\Bonus\Model
 */
class Bonus
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var int
     */
    protected $promo;

    /**
     * @var int
     */
    protected $amountReal;

    /**
     * @var int
     */
    protected $amountBonus;

    /**
     * @var CurrencyInterface
     */
    protected $currency;

    /**
     * Bonus constructor.
     * @param WalletAwareInterface $userInterface
     */
    public function __construct(WalletAwareInterface $userInterface)
    {
        $this->createdAt = new \DateTime();
        $this->user = $userInterface;
        $this->currency = $userInterface->getWallet()->getCurrency();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getPromo(): int
    {
        return $this->promo;
    }

    /**
     * @param int $promo
     * @return BonusInterface
     */
    public function setPromo(int $promo): BonusInterface
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmountReal(): int
    {
        return $this->amountReal;
    }

    /**
     * @param int $amountReal
     * @return BonusInterface
     */
    public function setAmountReal(int $amountReal): BonusInterface
    {
        $this->amountReal = $amountReal;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmountBonus(): int
    {
        return $this->amountBonus;
    }

    /**
     * @param int $amountBonus
     * @return BonusInterface
     */
    public function setAmountBonus(int $amountBonus): BonusInterface
    {
        $this->amountBonus = $amountBonus;

        return $this;
    }

    /**
     * @return CurrencyInterface
     */
    public function getCurrency(): CurrencyInterface
    {
        return $this->currency;
    }

}
