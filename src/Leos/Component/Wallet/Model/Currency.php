<?php
namespace Leos\Component\Wallet\Model;

use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Wallet\Definition\CurrencyInterface;

/**
 * Class Currency
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Model
 */
class Currency implements CurrencyInterface
{
    use TimestampableTrait;
    
    /**
     * @var string
     */
    protected $code;

    /**
     * @var float
     */
    protected $exchange;

    /**
     * Currency constructor.
     * @param string $code
     * @param float $exchange
     */
    public function __construct(string $code = 'EUR', float $exchange = 1.00)
    {
        $this->code = $code;
        $this->exchange = $exchange;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->exchange;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->code;
    }
}
