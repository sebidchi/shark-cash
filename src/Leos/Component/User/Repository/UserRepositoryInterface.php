<?php
namespace Leos\Component\User\Repository;

use Leos\Component\User\Definition\UserInterface;

/**
 * Class UserRepositoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Repository
 */
interface UserRepositoryInterface
{
    /**
     * @param array $criteria
     * @param array $sort
     * @return mixed
     */
    public function findAll(array $criteria = [], array $sort = []);

    /**
     * @param string $slug
     * @return UserInterface
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySlug(string $slug);

    /**
     * @param string $username
     * @return UserInterface
     */
    public function findOneByUsername(string $username);

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function save(UserInterface $user): UserInterface;
}
