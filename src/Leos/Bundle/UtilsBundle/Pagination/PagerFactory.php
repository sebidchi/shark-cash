<?php
namespace Leos\Bundle\UtilsBundle\Pagination;

use Hateoas\Configuration\Route;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Hateoas\Representation\PaginatedRepresentation;
use Pagerfanta\Pagerfanta;

/**
 * Class PagerFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UtilsBundle\Pagination
 */
class PagerFactory
{
    const
        DEFAULT_LIMIT = 500
    ;

    /**
     * @var PagerFactory
     */
    private $pagerFactory;

    /**
     * PagerFactory constructor.
     */
    public function __construct()
    {
        $this->pagerFactory = new PagerfantaFactory();
    }

    /**
     * @param Pagerfanta $pagination
     * @param string $route
     * @param array $params
     * @param int $limit
     * @param int $page
     * @return PaginatedRepresentation
     */
    public function paginate(
        Pagerfanta $pagination,
        string $route,
        array $params = [],
        int $limit = self::DEFAULT_LIMIT,
        int $page = 1): PaginatedRepresentation
    {

        $pagination
            ->setMaxPerPage($limit)
            ->setCurrentPage($page)
        ;

        return (new PagerfantaFactory())
            ->createRepresentation(
                $pagination,
                new Route(
                    $route,
                    array_merge($params, [
                        'limit' => $limit,
                        'page' => $page
                    ]),
                    true
                )
            );
    }
}

