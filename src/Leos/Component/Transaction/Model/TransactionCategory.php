<?php
namespace Leos\Component\Transaction\Model;

use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Transaction\Definition\TransactionCategoryInterface;

/**
 * Class TransactionCategory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Model
 */
class TransactionCategory implements TransactionCategoryInterface
{
    const
        BET = 'bet',
        WIN = 'win',
        ROLLBACK_BET = 'rollback_bet',
        ROLLBACK_WIN = 'rollback_win',

        BONUS_TURN_TO_REAL = 'bonus_turn_to_real',

        DEPOSIT = 'deposit',
        WITHDRAWAL = 'withdrawal',
        CANCEL_DEPOSIT = 'cancel_deposit',
        CANCEL_WITHDRAWAL = 'cancel_withdrawal',

        MANUAL_CREDIT = 'manual_credit',
        MANUAL_DEBIT = 'manual_debit'
    ;

    const
        CREDIT = 'credit',
        DEBIT  = 'debit'
    ;

    const
        TYPE_CREDIT = [
            self::WIN,
            self::ROLLBACK_BET,
            self::BONUS_TURN_TO_REAL,
            self::DEBIT,
            self::CANCEL_WITHDRAWAL,
            self::MANUAL_CREDIT
        ],
        TYPE_DEBIT = [
            self::BET,
            self::ROLLBACK_WIN,
            self::WITHDRAWAL,
            self::CANCEL_DEPOSIT,
            self::MANUAL_DEBIT
        ]
    ;


    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * TransactionCategory constructor.
     * @param string $name
     */
    public function __construct(string $name = '')
    {
        $this->name = $name;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TransactionCategory
     */
    public function setName($name): TransactionCategory
    {
        if (!$this->isValid($name)) {

            throw new \LogicException("category.exception.invalid_type");
        }

        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        if (in_array($this->name, self::TYPE_CREDIT)) {

            return self::CREDIT;
        }

        if (in_array($this->name, self::TYPE_DEBIT)) {

            return self::DEBIT;
        }

        throw new \LogicException("category.exception.unknown_type");
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function isValid(string $name):  bool
    {
        $types = (new \ReflectionClass(__CLASS__))->getConstants();

        return in_array(strtolower($name), $types);
    }



}