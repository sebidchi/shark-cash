<?php
namespace Leos\Component\Transaction\Factory;

use Leos\Component\Transaction\Definition\MovementInterface;

/**
 * Class MovementFactoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Factory
 */
interface MovementFactoryInterface
{

    /**
     * @param string $category
     * @param float $real
     * @param float $bonus
     *
     * @return MovementInterface
     */
    public function create(string $category, float $real = 0.00, float $bonus = 0.00): MovementInterface;

}