<?php
namespace Leos\Component\Play\Repository;

use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Model\PlaySessionRound;

/**
 * Class PlaySessionRoundRepositoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Repository
 */
interface PlaySessionRoundRepositoryInterface
{

    /**
     * @param PlaySession $session
     * @param string $id
     *
     * @return null|PlaySessionRound
     */
        public function findOneBySessionAndRound(PlaySession $session, string $id);
}