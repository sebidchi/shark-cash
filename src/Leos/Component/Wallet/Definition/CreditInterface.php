<?php

namespace Leos\Component\Wallet\Definition;

/**
 * Class CreditInterface
 * A Credit object is a Movement with amount * 100
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Definition
 */
interface CreditInterface
{

    /**
     * CreditInterface constructor.
     * @param float $amount
     */
    public function __construct(float $amount);

    /**
     * @return int
     */
    public function getAmount(): int;

}
