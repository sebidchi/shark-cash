<?php
namespace Leos\Bundle\UserBundle\Repository;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\User\Repository\UserRepositoryInterface;
use Leos\Bundle\UtilsBundle\Doctrine\ORM\Repository\EntityRepository;

use Pagerfanta\Pagerfanta;

/**
 * Class UserRepository
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UserBundle\Repository
 */
class UserRepository extends EntityRepository implements UserRepositoryInterface
{
    /**
     * @param array $criteria
     * @param array $sort
     * @return array|Pagerfanta
     */
    public function findAll(array $criteria = [], array $sort = [])
    {
        return
            $this
                ->createPaginator('u', $criteria, $sort)
        ;
    }

    /**
     * @param string $slug
     * @return UserInterface
     */
    public function findOneBySlug(string $slug)
    {
        return $this
            ->createQueryBuilder('u')
            ->where('u.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

    }

    /**
     * @param string $username
     * @return UserInterface
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByUsername(string $username)
    {
        return $this
            ->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', strtolower($username))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function save(UserInterface $user): UserInterface
    {
        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }
}
