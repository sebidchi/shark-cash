<?php
namespace Leos\Component\Play\Repository;

use Leos\Component\Play\Model\PlaySession;
use Leos\Component\User\Definition\UserInterface;

/**
 * Class PlaySessionRepositoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Repository
 */
interface PlaySessionRepositoryInterface
{

    /**
     * @param array $criteria
     * @param array $sort
     * @return mixed
     */
    public function findAll(array $criteria = [], array $sort = []);

    /**
     * @param UserInterface $user
     * @param string $session
     *
     * @return null|PlaySession
     */
    public function findOneByUserAndSession(UserInterface $user, string $session);

    /**
     * @param UserInterface $user
     * @return mixed
     */
    public function findByUser(UserInterface $user);

    /**
     * @param int $id
     * @return null|PlaySession
     */
    public function findOneById(int $id);

    /**
     * @param PlaySession $session
     *
     * @return void
     */
    public function save(PlaySession $session);
}