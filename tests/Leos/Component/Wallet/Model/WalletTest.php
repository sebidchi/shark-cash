<?php

namespace Tests\Leos\Component\Wallet\Model;

use Leos\Component\Wallet\Model\Credit;
use Leos\Component\Wallet\Model\Wallet;
use Leos\Component\Wallet\Definition\WalletInterface;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class WalletTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Tests\Leos\Component\Wallet
 */
class WalletTest extends WebTestCase
{
    const
        USERNAME = 'Paco'
    ;

    public function testWallet()
    {
        $wallet = $this->getWallet();

        // ADD 30000 credits
        $wallet->addRealCredit(
            new Credit(300)
        );

        $this->assertEquals(30000, $wallet->getRealCredit());

        // REMOVE 15000 credits
        $wallet->removeRealCredit(
            new Credit(150)
        );

        $this->assertEquals(15000, $wallet->getRealCredit());

        // ADD 15000 bonus credits
        $wallet->addBonusCredit(
            new Credit(150)
        );

        $this->assertEquals(15000, $wallet->getBonusCredit());

        // REMOVE 15000 bonus credits
        $wallet->removeBonusCredit(
            new Credit(150)
        );

        $this->assertEquals(0, $wallet->getBonusCredit());


        $this->assertEquals(0, $wallet->setRealCredit(0)->getRealCredit());
        $this->assertEquals(0, $wallet->setBonusCredit(0)->getBonusCredit());
    }

    /**
     * @return WalletInterface
     */
    private function getWallet(): WalletInterface
    {
        return new Wallet(0, 0);
    }

}
