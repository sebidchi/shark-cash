<?php
namespace Leos\Component\Transaction\Exception;

use Leos\Component\I18n\Definition\TranslatableInterface;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\CreditInterface;

/**
 * Class NotEnoughFoundsException
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Exception
 */
class NotEnoughFoundsException extends \LogicException implements TranslatableInterface
{
    /**
     * @var CreditInterface
     */
    private $credit;

    /**
     * NotEnoughFoundsException constructor.
     * @param CreditInterface $credit
     */
    public function __construct(CreditInterface $credit)
    {
        parent::__construct('Not Enough Founds', 409);

        $this->credit = $credit;
    }

    /**
     * @return CreditInterface
     */
    public function getCredit(): CreditInterface
    {
        return $this->credit;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return 'transactions.exception.not_enough_founds';
    }


}