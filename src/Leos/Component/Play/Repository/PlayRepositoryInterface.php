<?php
namespace Leos\Component\Play\Repository;
use Leos\Component\Play\Model\PlaySession;
use Leos\Component\User\Model\User;

/**
 * Class PlayRepositoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Repository
 */
interface PlayRepositoryInterface
{
    /**
     * @param array $criteria
     * @param array $sort
     * @return mixed
     */
    public function findAll(array $criteria = [], array $sort = []);

    /**
     * @param User $user
     * @return mixed
     */
    public function findByUser(User $user);

    /**
     * @param PlaySession $session
     * @return mixed
     */
    public function findBySession(PlaySession $session);

    /**
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(int $id);
}