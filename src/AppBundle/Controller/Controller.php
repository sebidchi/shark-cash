<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package AppBundle\Controller
 */
class Controller extends FOSRestController
{
    /**
     * @param string $route
     * @param array $parameters
     * @param int $statusCode
     * @param array $headers
     * @return \FOS\RestBundle\View\View
     */
    protected function routeRedirectView(
        $route,
        array $parameters = [],
        $statusCode = Response::HTTP_CREATED,
        array $headers = [])
    {
        return parent::routeRedirectView(
            $route,
            array_merge([
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $parameters),
            $statusCode,
            $headers
        );
    }

    /**
     * @return mixed
     */
    protected function getFormat()
    {
        return $this->get('leos.utils.api')->getFormat();
    }

    /**
     * @return mixed
     */
    protected function getVersion()
    {
        return $this->get('leos.utils.api')->getVersion();
    }

}