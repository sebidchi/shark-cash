<?php
namespace Leos\Component\Transaction\Definition;

/**
 * Class TransactionAwareInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Definition
 */
interface TransactionAwareInterface
{

    /**
     * @return TransactionInterface
     */
    public function getTransaction(): TransactionInterface;

    /**
     * @param TransactionInterface $transactionInterface
     * @return mixed
     */
    public function setTransaction(TransactionInterface $transactionInterface);
}