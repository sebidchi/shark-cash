<?php
namespace Leos\Component\Transaction\Event;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;


/**
 * Interface TransactionFactoryEventInterface
 *
 * @package Leos\Component\Transaction\Event
 */
interface TransactionFactoryEventInterface
{
    const
        TRANSACTION_FACTORY = "leos.transaction.factory",
        TRANSACTION_FACTORY_PRE = "leos.transaction.factory.pre",
        TRANSACTION_FACTORY_POS = "leos.transaction.factory.pos";

    /**
     * TransactionFactoryEventInterface constructor.
     * @param UserInterface $user
     * @param MovementInterface $movementInterface
     * @param TransactionAwareInterface $transactionAwareInterface
     */
    public function __construct(UserInterface $user, MovementInterface $movementInterface, TransactionAwareInterface $transactionAwareInterface);

    /**
     * @return TransactionAwareInterface
     */
    public function getTransactionAware(): TransactionAwareInterface;

    /**
     * @return MovementInterface
     */
    public function getMovement(): MovementInterface;

    /**
     * @return WalletAwareInterface|UserInterface
     */
    public function getUser(): UserInterface;
}
