<?php
namespace Leos\Component\Play\Model;

use Leos\Bundle\TransactionBundle\Entity\TransactionCategory;
use Leos\Component\Play\Definition\PlayInterface;
use Leos\Component\Play\Exception\NoSessionInRoundException;
use Leos\Component\Play\Exception\RoundAlreadyClosedException;
use Leos\Component\User\Model\User;
use Leos\Component\Utils\DateTime\TimestampableTrait;

/**
 * Class PlaySessionRound
 *
 * @package Leos\Component\Play\Model
 */
class PlaySessionRound
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var PlaySession
     */
    protected $session;

    /**
     * @var Play[]
     */
    protected $playTransactions;

    /**
     * @var string
     */
    protected $roundId;

    /**
     * @var bool
     */
    protected $closed = false;

    /**
     * @var int
     */
    protected $betReal = 0;

    /**
     * @var int
     */
    protected $betBonus = 0;

    /**
     * @var int
     */
    protected $winReal = 0;

    /**
     * @var int
     */
    protected $winBonus = 0;

    /**
     * @var PlayInterface
     */
    private $currentPlay;

    /**
     * PlaySessionRound constructor.
     * @param string|null $round
     */
    public function __construct(string $round = null)
    {
        $this->roundId = $round;
        $this->createdAt = new \DateTime();
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return PlaySession
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param PlaySession $session
     * @return $this
     */
    public function setSession(PlaySession $session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * @param string $roundId
     * @return $this
     */
    public function setRoundId($roundId)
    {
        $this->roundId = $roundId;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isClosed()
    {
        return $this->closed;
    }

    /**
     * @return int
     */
    public function getBetReal()
    {
        return $this->betReal;
    }

    /**
     * @return int
     */
    public function getBetBonus()
    {
        return $this->betBonus;
    }

    /**
     * @return int
     */
    public function getWinReal()
    {
        return $this->winReal;
    }

    /**
     * @return int
     */
    public function getWinBonus()
    {
        return $this->winBonus;
    }

    /**
     * @param bool $closed
     * @return $this
     */
    public function setClosed(bool $closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * @return Play
     */
    public function getCurrentPlay()
    {
        return $this->currentPlay;
    }

    /**
     * @param PlayInterface $play
     * @return PlaySessionRound
     */
    public function addTransaction(PlayInterface $play): PlaySessionRound
    {
        if (!$this->getSession()) {

            throw new NoSessionInRoundException();
        }

        if ($this->isClosed()) {

            throw new RoundAlreadyClosedException();
        }

        $this->currentPlay = $play->setPlaySession($this->getSession());
        $this->currentPlay = $play->setPlaySessionRound($this);

        $transaction = $play->getTransaction();

        $this->user = $transaction->getUser();
        $this->roundId = $play->getRound();
        $this->closed = $play->isClose();

        if ($transaction->getCategory()->getName() == TransactionCategory::BET) {


            $this->betReal += $transaction->getAmountReal();
            $this->betBonus += $transaction->getAmountBonus();
        }

        if ($transaction->getCategory()->getName() == TransactionCategory::WIN) {

            $this->winReal += $transaction->getAmountReal();
            $this->winBonus += $transaction->getAmountBonus();
        }

        return $this;
    }
}
