<?php
namespace Leos\Bundle\TransactionBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Leos\Bundle\TransactionBundle\Entity\TransactionCategory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadTransactionCategoryData
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Tests\DataFixtures\ORM
 */
class LoadTransactionCategoryData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->container->getParameter('leos.fixtures.transaction_categories') as $name ) {

            $category = (new TransactionCategory($name));

            $manager->persist($category);
            $manager->flush();

            $this->addReference('transaction-category-' . $category->getName(), $category);
        }

    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
