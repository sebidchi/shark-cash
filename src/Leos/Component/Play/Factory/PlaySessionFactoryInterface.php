<?php
namespace Leos\Component\Play\Factory;

use Leos\Component\Play\Model\PlaySession;

/**
 * Class PlaySessionFactoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Factory
 */
interface PlaySessionFactoryInterface
{
    /**
     * @param array $data
     * @return PlaySession
     */
    public function create(array $data): PlaySession;
}