<?php
namespace Tests\Leos\Component\Transaction\Model;

use Leos\Component\Transaction\Model\CreditMovement;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class CreditMovementTest
 * 
 * @package Tests\Leos\Component\Transaction\Model
 */
class CreditMovementTest extends WebTestCase
{

    public function testCredit()
    {
        $credit = new CreditMovement(TransactionCategoryTest::getTransactionCategory("win"));

        // Real
        $this->assertEquals(0, $credit->setReal(0.00)->getReal()->getAmount());
        $this->assertEquals(1000, $credit->setReal(10.00)->getReal()->getAmount());
        $this->assertEquals(1050, $credit->setReal(10.50)->getReal()->getAmount());
        // Bonus
        $this->assertEquals(0, $credit->setBonus(0.00)->getBonus()->getAmount());
        $this->assertEquals(1000, $credit->setBonus(10.00)->getBonus()->getAmount());
        $this->assertEquals(1050, $credit->setBonus(10.50)->getBonus()->getAmount());
    }
}
