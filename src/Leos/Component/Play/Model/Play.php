<?php
namespace Leos\Component\Play\Model;

use Leos\Component\Play\Definition\PlayInterface;
use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Transaction\Model\TransactionCategory;
use Leos\Component\Transaction\Definition\TransactionInterface;

/**
 * Class Play
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Model
 */
class Play implements PlayInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * @var int
     */
    protected $game = 0;

    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var PlaySession
     */
    protected $playSession;

    /**
     * @var string
     */
    protected $round = '';

    /**
     * @var PlaySessionRound
     */
    protected $playSessionRound;

    /**
     * Bet to balance in real
     *
     * @var float
     */
    protected $b2bReal = 0;

    /**
     * Bet to balance in bonus
     *
     * @var float
     */
    protected $b2bBonus = 0;

    /**
     * Transaction close round
     *
     * @var bool
     */
    protected $close = false;

    /**
     * @var TransactionInterface
     */
    protected $transaction;

    /**
     * Play constructor.
     *
     * @param int $game
     * @param string $session
     * @param string $round
     */
    public function __construct(int $game = 1, string $session = '', string $round = '')
    {
        $this->createdAt = new \DateTime();
        $this->game = $game;
        $this->session = $session;
        $this->round = $round;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getGame(): int
    {
        return $this->game;
    }

    /**
     * @return string
     */
    public function getSession(): string
    {
        return $this->session;
    }

    /**
     * @param $session
     * @return PlayInterface
     */
    public function setSession($session = ''): PlayInterface
    {
        $this->session = $session;
        return $this;
    }

    /**
     * @return PlaySession
     */
    public function getPlaySession()
    {
        return $this->playSession;
    }

    /**
     * @param PlaySession $playSession
     * @return PlayInterface
     */
    public function setPlaySession(PlaySession $playSession): PlayInterface
    {
        $this->playSession = $playSession;

        return $this;
    }

    /**
     * @return string
     */
    public function getRound(): string
    {
        return $this->round;
    }

    /**
     * @param string $round
     * @return PlayInterface
     */
    public function setRound($round = ''): PlayInterface
    {
        $this->round = $round;
        return $this;
    }

    /**
     * @return PlaySessionRound
     */
    public function getPlaySessionRound()
    {
        return $this->playSessionRound;
    }

    /**
     * @param PlaySessionRound $playSessionRound
     * @return PlayInterface
     */
    public function setPlaySessionRound(PlaySessionRound $playSessionRound): PlayInterface
    {
        $this->playSessionRound = $playSessionRound;

        return $this;
    }

    /**
     * @return TransactionInterface
     */
    public function getTransaction(): TransactionInterface
    {
        return $this->transaction;
    }

    /**
     * @param TransactionInterface $transaction
     * @return PlayInterface
     */
    public function setTransaction(TransactionInterface $transaction): PlayInterface
    {
        $this->transaction = $transaction;

        if (TransactionCategory::BET == $transaction->getCategory()->getName()) {

            $this->b2bReal = $transaction->getAmountReal() / (($transaction->getAmountRealOld() == 0) ? 1 : $transaction->getAmountRealOld());
            $this->b2bBonus = $transaction->getAmountBonus() / (($transaction->getAmountBonusOld() == 0) ? 1 : $transaction->getAmountBonusOld());
        }

        return $this;
    }

    /**
     * @return float
     */
    public function getB2bReal(): float
    {
        return $this->b2bReal;
    }

    /**
     * @return float
     */
    public function getB2bBonus(): float
    {
        return $this->b2bBonus;
    }

    /**
     * @return boolean
     */
    public function isClose(): bool
    {
        return $this->close;
    }

    /**
     * @param bool $close
     * @return PlayInterface
     */
    public function setClose(bool $close): PlayInterface
    {
        $this->close = $close;

        return $this;
    }


}
