<?php
namespace Leos\Bundle\PlayBundle\Factory;

use Symfony\Component\Form\FormFactory;

use Leos\Component\Utils\Factory\AbstractFactory;
use Leos\Component\Play\Model\PlaySessionRound as RoundModel;
use Leos\Component\Play\Factory\PlaySessionRoundFactoryInterface;

use Leos\Bundle\PlayBundle\Entity\PlaySessionRound;
use Leos\Bundle\PlayBundle\Form\PlaySessionRoundType;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;

/**
 * Class PlaySessionRoundFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Factory
 */
class PlaySessionRoundFactory implements PlaySessionRoundFactoryInterface
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * PlaySessionRoundFactory constructor.
     *
     * @param FormFactory $formFactory
     */
    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param array $data
     *
     * @return RoundModel
     * @throws FormException
     */
    public function create(array $data): RoundModel
    {
        return $this->build($data, new PlaySessionRound(), AbstractFactory::CREATE);
    }

    /**
     * @param array $data
     * @param PlaySessionRound $round
     * @param string $method
     *
     * @return PlaySessionRound
     * @throws FormException
     */
    private function build(array $data, PlaySessionRound $round, string $method): PlaySessionRound
    {
        $form = $this->getForm($data, $round, $method);

        if (!$form->isValid()) {

            throw (new FormException())->setForm($form);
        }

        return $round;
    }

    /**
     * @param array $data
     * @param PlaySessionRound $session
     * @param string $method
     *
     * @return $this|\Symfony\Component\Form\FormInterface
     */
    private function getForm(array $data, PlaySessionRound $session, string $method)
    {
        return $this->formFactory
            ->create(PlaySessionRoundType::class, $session, ['method' => $method])
            ->submit($data, AbstractFactory::UPDATE !== $method);
    }
}