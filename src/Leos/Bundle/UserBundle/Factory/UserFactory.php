<?php
namespace Leos\Bundle\UserBundle\Factory;

use Leos\Component\Utils\Factory\AbstractFactory;

use Leos\Bundle\UserBundle\Entity\User;
use Leos\Bundle\UserBundle\Form\UserType;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UserBundle\Factory
 */
class UserFactory extends AbstractFactory
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * UserFactory constructor.
     * @param FormFactory $formFactory
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(FormFactory $formFactory, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->formFactory = $formFactory;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param array $data
     * @return User
     * @throws FormException
     */
    public function create(array $data): User
    {
        return $this->build($data, self::CREATE, new User());
    }

    /**
     * @param array $data
     * @param $object
     * @return User
     * @throws FormException
     */
    public function replace(array $data, $object): User
    {
        return $this->build($data, self::REPLACE, $object);
    }

    /**
     * @param array $data
     * @param $object
     * @return User
     * @throws FormException
     */
    public function update(array $data, $object): User
    {
        return $this->build($data, self::UPDATE, $object);
    }

    /**
     * @param array $data
     * @param string $method
     * @param User|null $user
     * @return User
     * @throws FormException
     */
    protected function build(array $data, string $method = self::CREATE, User $user): User
    {
        $form = $this->getForm($user, $data, $method);

        if (!$form->isValid()) {

            throw (new FormException())->setForm($form);
        }

        return ($user->getPlainPassword())
            ? $this->updatePassword($user)
            : $user;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    private function updatePassword(User $user): User
    {
        $password = $this->userPasswordEncoder
            ->encodePassword($user, $user->getPlainPassword());

        $user->setPassword($password);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @param string $method
     * @return FormInterface
     */
    protected function getForm(User $user, array $data, string $method): FormInterface
    {
        $form = $this->formFactory
            ->createBuilder(UserType::class, $user, ['method' => $method])
            ->getForm();

        return $form->submit($data, self::UPDATE !== $method);
    }

}
