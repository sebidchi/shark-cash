<?php
namespace Leos\Bundle\TransactionBundle\Event;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;
use Leos\Component\Transaction\Event\TransactionFactoryEventInterface;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class TransactionFactoryEvent
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Event
 */
final class TransactionFactoryEvent extends Event implements TransactionFactoryEventInterface
{
    /**
     * @var WalletAwareInterface|UserInterface
     */
    private $user;

    /**
     * @var MovementInterface
     */
    private $movement;

    /**
     * @var TransactionAwareInterface
     */
    private $transactionAware;

    /**
     * TransactionFactoryEvent constructor.
     *
     * @param UserInterface $user
     * @param MovementInterface $movementInterface
     * @param TransactionAwareInterface $transactionAwareInterface
     */
    public function __construct(
        UserInterface $user,
        MovementInterface $movementInterface,
        TransactionAwareInterface $transactionAwareInterface)
    {
        $this->transactionAware = $transactionAwareInterface;
        $this->movement = $movementInterface;
        $this->user = $user;
    }

    /**
     * @return TransactionAwareInterface
     */
    public function getTransactionAware(): TransactionAwareInterface
    {
        return $this->transactionAware;
    }

    /**
     * @return MovementInterface
     */
    public function getMovement(): MovementInterface
    {
        return $this->movement;
    }

    /**
     * @return WalletAwareInterface|UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }
}
