<?php
namespace Leos\Component\User\Manager;

use Leos\Component\User\Model\User;
use Leos\Component\User\Definition\UserInterface;

/**
 * Class UserManagerInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Manager
 */
interface UserManagerInterface
{
    /**
     * @param array $criteria
     * @param array $sort
     * @return mixed
     */
    public function all(array $criteria = [], array $sort = []);

    /**
     * @param string $slug
     * @return UserInterface
     */
    public function findOneBySlug(string $slug);

    /**
     * @param array $data
     *
     * @return User
     */
    public function create(array $data): User;

    /**
     * @param array $data
     * @param string $slug
     * @return User
     */
    public function update(array $data, string $slug): User;

    /**
     * @param array $data
     * @param string $slug
     * @return User
     */
    public function replace(array $data, string $slug): User;

    /**
     * @param string $slug
     * @return mixed
     */
    public function delete(string $slug);
}
