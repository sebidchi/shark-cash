<?php
namespace Leos\Component\Transaction\Definition;

use Leos\Component\Transaction\Model\TransactionCategory;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\CurrencyInterface;

/**
 * Class TransactionInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Definition
 */
interface TransactionInterface
{

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface;

    /**
     * @return TransactionCategory
     */
    public function getCategory(): TransactionCategory;

    /**
     * @return int
     */
    public function getAmountReal(): int;

    /**
     * @return int
     */
    public function getAmountRealOld(): int;

    /**
     * @return int
     */
    public function getAmountBonus(): int;

    /**
     * @return int
     */
    public function getAmountBonusOld(): int;

    /**
     * @return int
     */
    public function getAmountRealOperation(): int;

    /**
     * @return int
     */
    public function getAmountBonusOperation(): int;

    /**
     * @return CurrencyInterface
     */
    public function getCurrency(): CurrencyInterface;
}