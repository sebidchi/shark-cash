<?php

namespace Leos\Bundle\TransactionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation\Slug;
use Gedmo\Mapping\Annotation\Timestampable;
use Leos\Component\Transaction\Model\TransactionCategory as BaseModel;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TransactionCategory
 *
 * @ORM\Table(name="transaction_category")
 * @ORM\Entity(repositoryClass="Leos\Bundle\TransactionBundle\Repository\TransactionCategoryRepository")
 */
class TransactionCategory extends BaseModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttribute
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $id = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     *
     * @Slug(fields={"name"})
     *
     * @Assert\NotBlank(message="transaction_category.name.not_blank")
     * @Serializer\XmlAttribute
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $name = "";


    /**
     * Creation date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $createdAt;

    /**
     * Last update date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Timestampable()
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $updatedAt;

    /**
     * Delete date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $deletedAt;
}

