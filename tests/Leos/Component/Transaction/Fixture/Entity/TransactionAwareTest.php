<?php
namespace Tests\Leos\Component\Transaction\Fixture\Entity;

use Leos\Component\Transaction\Definition\TransactionInterface;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;

/**
 * Class TransactionAwareTest
 * @package Leos\Component\Transaction\Fixture\Entity
 */
class TransactionAwareTest implements TransactionAwareInterface
{
    /**
     * @var TransactionInterface
     */
    private $transaction;

    /**
     * @return TransactionInterface
     */
    public function getTransaction(): TransactionInterface
    {
        return $this->transaction;
    }

    /**
     * @param TransactionInterface $transactionInterface
     * @return $this
     */
    public function setTransaction(TransactionInterface $transactionInterface)
    {
        $this->transaction = $transactionInterface;

        return $this;
    }

}
