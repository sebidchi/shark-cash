<?php
namespace Leos\Bundle\UtilsBundle\EventListener;

use FOS\RestBundle\View\View;
use FOS\RestBundle\FOSRestBundle;
use FOS\RestBundle\View\ViewHandlerInterface;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Templating\TemplateReferenceInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

/**
 * Class FOSViewResponseListener
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UtilsBundle\EventListener
 */
class FOSViewResponseListener implements EventSubscriberInterface
{
    /**
     * @var ViewHandlerInterface
     */
    private $viewHandler;

    /**
     * @var bool
     */
    private $forceView;

    /**
     * Constructor.
     *
     * @param ViewHandlerInterface $viewHandler
     * @param bool                 $forceView
     */
    public function __construct(ViewHandlerInterface $viewHandler, $forceView)
    {
        $this->viewHandler = $viewHandler;
        $this->forceView = $forceView;
    }

    /**
     * Renders the parameters and template and initializes a new response object with the
     * rendered content.
     *
     * @param GetResponseForControllerResultEvent $event
     * @return bool|null
     */
    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();

        if (!$request->attributes->get(FOSRestBundle::ZONE_ATTRIBUTE, true)) {
            return false;
        }

        $configuration = $request->attributes->get('_template');

        $view = $event->getControllerResult();

        if (!$view instanceof View) {

            if (!$configuration instanceof ViewAnnotation && !$this->forceView) {

                return null;
            }
            $view = new View($view);
        }

        if ($configuration instanceof ViewAnnotation) {

            if ($configuration->getTemplateVar()) {

                $view->setTemplateVar($configuration->getTemplateVar());
            }
            if (null !== $configuration->getStatusCode()
                && (null === $view->getStatusCode()
                    || Response::HTTP_OK === $view->getStatusCode())) {

                $view->setStatusCode($configuration->getStatusCode());
            }

            $context = $view->getContext();

            if ($configuration->getSerializerGroups()) {

                $context->addGroups($configuration->getSerializerGroups());
            }

            if ($configuration->getSerializerEnableMaxDepthChecks()) {

                $context->setMaxDepth(0);
            }

            list($controller, $action) = $configuration->getOwner();
            $vars = $this->getDefaultVars($configuration, $controller, $action);

        } else {

            $vars = null;
        }

        if (null === $view->getFormat()) {

            $view->setFormat($request->getRequestFormat());
        }


        if ($version = $request->get('version', false)) {

            $view->getContext()->setVersion($version);
        }

        if ($this->viewHandler->isFormatTemplating($view->getFormat())
            && !$view->getRoute()
            && !$view->getLocation()
        ) {

            if (null !== $vars && 0 !== count($vars)) {

                $parameters = (array) $this->viewHandler->prepareTemplateParameters($view);

                foreach ($vars as $var) {

                    if (!array_key_exists($var, $parameters)) {

                        $parameters[$var] = $request->attributes->get($var);
                    }
                }

                $view->setData($parameters);
            }

            if ($configuration && ($template = $configuration->getTemplate()) && !$view->getTemplate()) {

                if ($template instanceof TemplateReferenceInterface) {

                    $template->set('format', null);
                }

                $view->setTemplate($template);
            }
        }

        $response = $this->viewHandler->handle($view, $request);
        $event->setResponse($response);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // Must be executed before SensioFrameworkExtraBundle's listener
        return array(
            KernelEvents::VIEW => array('onKernelView', 30),
        );
    }

    /**
     * @param Template $template
     * @param object   $controller
     * @param string   $action
     *
     * @return array
     *
     * @see \Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener::resolveDefaultParameters()
     */
    private function getDefaultVars(Template $template = null, $controller, $action)
    {
        if (0 !== count($arguments = $template->getVars())) {

            return $arguments;
        }

        if (!$template instanceof ViewAnnotation || $template->isPopulateDefaultVars()) {
            $r = new \ReflectionObject($controller);
            $arguments = array();

            foreach ($r->getMethod($action)->getParameters() as $param) {

                $arguments[] = $param->getName();
            }

            return $arguments;
        }
    }
}