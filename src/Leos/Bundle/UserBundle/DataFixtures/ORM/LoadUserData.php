<?php
namespace Leos\Bundle\UserBundle\DataFixtures\ORM;

use Leos\Bundle\UserBundle\Entity\User;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UserBundle\DataFixtures
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $manager = $this->container->get('leos.manager.user');

        foreach ($this->container->getParameter('leos.fixtures.users') as $name => $data) {

            $data['username'] = $name;
            $data['currency'] = $this->getReference('currency-' . $data['currency'])->getId();

            /** @var User $user */
            $user = $manager->create($data);

            $this->addReference('user-' . $user->getSlug(), $user);
        }

    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
