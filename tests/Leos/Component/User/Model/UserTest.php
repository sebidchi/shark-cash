<?php

namespace Tests\Leos\Component\User\Model;

use Leos\Component\User\Model\User;
use Leos\Component\User\Definition\UserInterface;

use Tests\Leos\Component\Wallet\Model\CurrencyTest;
use Tests\Leos\Component\User\Fixture\UserWalletAwareTest;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class UserTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Model
 */
class UserTest extends WebTestCase
{
    const
        USERNAME = 'Paco',
        CURRENCY = 'eur',
        ROLES = [
            'ROLE_USER',
            'ROLE_MASTER'
        ]
    ;

    public function testUser()
    {
        $user = $this->getUser();

        $this->assertTrue(is_string($user->getSalt()));
        $this->assertNotNull($user->getSlug());
        $this->assertNotNull($user->getCreatedAt());
        $this->assertNull($user->getUpdatedAt());
        $this->assertNull($user->getDeletedAt());
        $this->assertNotNull($user->getCreatedAt());
        $this->assertEquals(self::ROLES, $user->getRoles());
        $this->assertEquals([], $user->setRoles([])->getRoles());

        $this->assertNotNull($user->setPassword("test")->getPassword());
        $this->assertEquals(self::USERNAME, $user->getUsername());
        $this->assertEquals("Paco2", $user->setUsername("Paco2")->getUsername());
    }

    /**
     * @group unit
     */
    public function testUserIdException()
    {
        $user = $this->getUser();

        $this->assertEquals(0, $user->getId());
    }

    /**
     * @return User|UserInterface
     */
    public static function getUser(): UserInterface
    {
        return (new UserWalletAwareTest(self::USERNAME, self::ROLES))->setCurrency(CurrencyTest::getCurrency(self::CURRENCY, 1));
    }

}
