<?php
namespace Leos\Bundle\UserBundle\Entity;

use Leos\Component\User\Model\User as BaseUser;
use Leos\Component\Wallet\Definition\CurrencyInterface;
use Leos\Component\Wallet\Definition\WalletInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;

use Symfony\Component\Security\Core\User\UserInterface;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UserBundle\Entity
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Leos\Bundle\UserBundle\Repository\UserRepository")
 *
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "get_user",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "slug" = "expr(object.getSlug())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"List"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "play",
 *     href = @Hateoas\Route(
 *          "get_user_plays",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "slug" = "expr(object.getSlug())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"Basic"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "sessions",
 *     href = @Hateoas\Route(
 *          "get_user_playsessions",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "slug" = "expr(object.getSlug())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"Basic"}
 *     )
 * )
 *
 * @Serializer\ExclusionPolicy("all")
 */
class User extends BaseUser implements UserInterface, WalletAwareInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttribute
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $id = 0;

    /**
     * The user identifier
     *
     * @var string
     *
     * @ORM\Column(nullable=false, length=100, unique=true)
     *
     * @Assert\NotBlank(message="user.username.not_blank")
     * @Assert\NotNull(message="user.username.not_null")
     * @Assert\Length(
     *     min = 3,
     *     max = 100
     * )
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $username = '';

    /**
     * The user slug extract from username. It's unique
     *
     * @var string
     * @ORM\Column(nullable=false, length=100, unique=true)
     *
     * @Gedmo\Slug(fields={"username"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $slug;

    /**
     * The salt to use for hashing
     *
     * @var string
     * @ORM\Column(nullable=false, length=100)
     * @Assert\NotNull(message="user.salt.not_null")
     * @Serializer\Expose
     * @Serializer\Groups("SuperAdmin")
     */
    protected $salt = '';

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @ORM\Column(nullable=false, length=100)
     *
     * @Assert\NotNull(message="user.password.not_null")
     *
     * @Serializer\Expose
     * @Serializer\Groups("SuperAdmin")
     */
    protected $password = '';

    /**
     * @var string|null
     */
    private $plainPassword;

    /**
     * The current user state
     *
     * @var integer
     * @ORM\Column(nullable=false, type="integer")
     *
     * @Assert\NotNull(message="user.state.not_null")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $state = 1;

    /**
     * The List of user roles
     *
     * @var array
     *
     * @ORM\Column(type="array", nullable=false)
     *
     * @Assert\NotNull(message="user.roles.not_null")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $roles = [];

    /**
     * The user Wallet object
     *
     * @var WalletInterface
     *
     * @ORM\Embedded(class="Wallet")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $wallet;

    /**
     * @var CurrencyInterface
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     *
     * @Assert\NotBlank(message="user.currency.not_blank")
     * @Assert\NotNull(message="user.currency.not_null")
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\I18nBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    protected $currency;

    /**
     * Creation date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Assert\NotNull(message="user.created_at.not_null")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $createdAt;

    /**
     * Last update date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $updatedAt;

    /**
     * Delete date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $deletedAt;

    /**
     * User constructor.
     *
     * @param string $username
     * @param array $roles
     */
    public function __construct(string $username = null, array $roles = [])
    {
        parent::__construct($username, $roles);

        $this->wallet = new Wallet();
    }

    /**
     * @return string|null
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPlainPassword(string $password)
    {
        $this->plainPassword = $password;

        return $this;
    }

    /**
     * @return WalletInterface
     */
    public function getWallet(): WalletInterface
    {
        return $this->wallet;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return User
     */
    public function setState($state): User
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return int
     */
    public function getReal()
    {
        return $this->wallet->getRealCredit();
    }

    /**
     * @return int
     */
    public function getBonus()
    {
        return $this->wallet->getBonusCredit();
    }

    /**
     * @return mixed
     */
    public function getIndexCurrency()
    {
        return $this->currency->getCode();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

}
