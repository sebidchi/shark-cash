<?php

namespace Leos\Bundle\I18nBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Slug;
use Leos\Component\Wallet\Model\Currency as BaseCurrency;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Currency
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="Leos\Bundle\I18nBundle\Repository\CurrencyRepository")
 */
class Currency extends BaseCurrency
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    private $id;

    /**
     * @var string
     *
     * @Slug(fields={"code"})
     * @ORM\Column(type="string", length=3, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=5, scale=4)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $exchange;


    /**
     * Creation date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $createdAt;

    /**
     * Last update date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $updatedAt;

    /**
     * Delete date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     */
    protected $deletedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}

