<?php
namespace Leos\Bundle\TransactionBundle\Manager;
use Leos\Bundle\TransactionBundle\Repository\TransactionCategoryRepository;
use Leos\Bundle\TransactionBundle\Repository\TransactionRepository;
use Leos\Component\Transaction\Factory\MovementFactoryInterface;
use Leos\Component\Transaction\Model\Transaction;

/**
 * Class TransactionManager
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\TransactionBundle\Manager
 */
class TransactionManager
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var TransactionCategoryRepository
     */
    private $categoryRepository;

    /**
     * @var MovementFactoryInterface
     */
    private $movementFactory;

    /**
     * TransactionManager constructor.
     * @param TransactionRepository $transactionRepository
     * @param TransactionCategoryRepository $categoryRepository
     * @param MovementFactoryInterface $movementFactory
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        TransactionCategoryRepository $categoryRepository,
        MovementFactoryInterface $movementFactory
    )
    {

        $this->transactionRepository = $transactionRepository;
        $this->categoryRepository = $categoryRepository;
        $this->movementFactory = $movementFactory;
    }

    /**
     * @param array $criteria
     * @param array $sort
     * @return \Pagerfanta\Pagerfanta
     */
    public function all(array $criteria = [], array $sort = [])
    {
        return $this->transactionRepository->findAll($criteria, $sort);
    }

    /**
     * @param int $id
     * @param \Exception $e
     * @return Transaction
     * @throws \Exception
     */
    public function findOneByIdOrFail(int $id, \Exception $e): Transaction
    {
        $transaction = $this->transactionRepository->findOneById($id);

        if (!$transaction) {

            throw $e;
        }

        return $transaction;
    }

    /**
     * @param int $id
     * @return Transaction
     */
    public function findOneById(int $id)
    {
        return $this->transactionRepository->findOneById($id);
    }
}