Shack-cash
===========

Run:
    
    1 - Add sharkcash.local to /etc/hosts
    
    2 - docker-compose up -d
    
    3 - docker exec -it shark_php composer install
    
    4 - Install assets runing: docker run -it -v $PWD:/app jorge07/node-bower-grunt bom i && bower i --allow-root && grunt build
    
    5 - Go sharkcash.local/app_dev.php (Kibana running on port 81)
    
    6 - Run Behat tests: docker exec -it shark_php vendor/bin/behat