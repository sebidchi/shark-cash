<?php
namespace Leos\Component\Play\Model;

use Leos\Component\Play\Definition\PlayInterface;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Transaction\Model\TransactionCategory;

/**
 * Class PlaySession
 *
 * @package Leos\Component\Play\Model
 */
class PlaySession
{
    use TimestampableTrait;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var int
     */
    protected $game;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var int
     */
    protected $provider;

    /**
     * @var string
     */
    protected $session;

    /**
     * @var int
     */
    protected $bets = 0;

    /**
     * @var int
     */
    protected $wins = 0;

    /**
     * @var int
     */
    protected $betsAmountReal;

    /**
     * @var int
     */
    protected $betsAmountBonus;

    /**
     * @var int
     */
    protected $winsAmountReal;

    /**
     * @var int
     */
    protected $winsAmountBonus;

    /**
     * @var PlaySessionRound[]
     */
    protected $rounds = [];

    /**
     * @var array
     */
    protected $playTransactions = [];

    /**
     * @var Play
     */
    private $currentPlay;

    /**
     * PlaySession constructor.
     *
     * @param string|null $session
     */
    public function __construct(string $session = null)
    {
        $this->session = $session;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param string $session
     * @return $this
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return int
     */
    public function getBets()
    {
        return $this->bets;
    }
    /**
     * @return int
     */
    public function getWins()
    {
        return $this->wins;
    }

    /**
     * @return int
     */
    public function getBetsAmountReal()
    {
        return $this->betsAmountReal;
    }

    /**
     * @return int
     */
    public function getBetsAmountBonus()
    {
        return $this->betsAmountBonus;
    }

    /**
     * @return int
     */
    public function getWinsAmountReal()
    {
        return $this->winsAmountReal;
    }

    /**
     * @return int
     */
    public function getWinsAmountBonus()
    {
        return $this->winsAmountBonus;
    }

    /**
     * @return array
     */
    public function getPlayTransactions()
    {
        return $this->playTransactions;
    }

    /**
     * @param PlayInterface $play
     * @return $this
     */
    protected function addPlayTransaction(PlayInterface $play)
    {
        $this->playTransactions[] = $play->setPlaySession($this);

        $this->resume($play);

        return $this;
    }

    private function resume(PlayInterface $play)
    {
        $this->currentPlay = $play;
        $this->session = $play->getSession();
        $this->game = $play->getGame();

        if (null == $this->user) {

            $this->user = $play->getTransaction()->getUser();

        } elseif ($this->user->getId() != $play->getTransaction()->getUser()->getId() ) {

            throw new \LogicException("play_session.exception.user_can_not_change");
        }

        $transaction = $play->getTransaction();

        if ($transaction->getCategory()->getName() == TransactionCategory::BET) {

            $this->bets++;
            $this->betsAmountReal += $transaction->getAmountReal();
            $this->betsAmountBonus += $transaction->getAmountBonus();
        }

        if ($transaction->getCategory()->getName() == TransactionCategory::ROLLBACK_BET) {

            $this->bets--;
            $this->betsAmountReal -= $transaction->getAmountReal();
            $this->betsAmountBonus -= $transaction->getAmountBonus();
        }

        if ($transaction->getCategory()->getName() == TransactionCategory::WIN) {

            $this->wins++;
            $this->winsAmountReal += $transaction->getAmountReal();
            $this->winsAmountBonus += $transaction->getAmountBonus();
        }

        if ($transaction->getCategory()->getName() == TransactionCategory::ROLLBACK_WIN) {

            $this->wins--;
            $this->winsAmountReal -= $transaction->getAmountReal();
            $this->winsAmountBonus -= $transaction->getAmountBonus();
        }

    }

    /**
     * @return Play
     */
    public function getCurrentPlay(): Play
    {
        return $this->currentPlay;
    }

    /**
     * @return PlaySessionRound[]
     */
    public function getRounds()
    {
        return $this->rounds;
    }

    /**
     * @param string $roundId
     * @return PlaySessionRound
     */
    public function getRound(string $roundId): PlaySessionRound
    {
        foreach ($this->rounds as $round) {

            if ($round->getRoundId() === $roundId) {

                return $round;
            }
        }

        return null;
    }

    /**
     * @param PlaySessionRound $round
     * @return $this
     */
    public function addRound(PlaySessionRound $round)
    {
        if (!in_array($round, $this->rounds)) {

            $this->rounds[] = $round->setSession($this);
        }

        if ($round->getCurrentPlay()) {

            $this->addPlayTransaction($round->getCurrentPlay());
        }

        return $this;
    }

    /**
     * @param PlayInterface $play
     * @return PlaySession
     */
    public function addTransaction(PlayInterface $play): PlaySession
    {
        if (!$this->getRound($play->getRound())) {

            throw new \LogicException('round.exception.not_in_session');
        }

        $this->getRound($play->getRound())->addTransaction($play);

        $this->addPlayTransaction($play);

        return $this;
    }


}
