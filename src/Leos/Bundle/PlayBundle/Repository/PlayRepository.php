<?php
namespace Leos\Bundle\PlayBundle\Repository;

use Leos\Component\User\Model\User;
use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Repository\PlayRepositoryInterface;

use Leos\Bundle\UtilsBundle\Doctrine\ORM\Repository\EntityRepository;

/**
 * Class PlayRepository
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Repository
 */
class PlayRepository extends EntityRepository implements PlayRepositoryInterface
{

    /**
     * @param array $criteria
     * @param array $sort
     * @return \Pagerfanta\Pagerfanta
     */
    public function findAll(array $criteria = [], array $sort = [])
    {
        return $this->createPaginator('p', $criteria, $sort);
    }

    /**
     * @param User $user
     * @return \Pagerfanta\Pagerfanta
     */
    public function findByUser(User $user)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('s, t, tc, c, sr')
            ->leftJoin('p.playSession', 's')
            ->leftJoin('p.transaction', 't')
            ->leftJoin('t.currency', 'c')
            ->leftJoin('t.category', 'tc')
            ->leftJoin('p.playSessionRound', 'sr')
            ->where('t.user = :user')
            ->setParameter('user', $user)
        ;

        return $this->getPaginator($qb);
    }

    /**
     * @param PlaySession $session
     * @return \Pagerfanta\Pagerfanta
     */
    public function findBySession(PlaySession $session)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('s, t, tc, c, sr')
            ->leftJoin('p.playSession', 's')
            ->leftJoin('p.transaction', 't')
            ->leftJoin('t.currency', 'c')
            ->leftJoin('t.category', 'tc')
            ->leftJoin('p.playSessionRound', 'sr')
            ->where('p.playSession = :session')
            ->setParameter('session', $session)
        ;

        return $this->getPaginator($qb);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(int $id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }
}
