<?php
namespace Leos\Component\Utils\Factory;

/**
 * Class AbstractFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Utils\Factory
 */
abstract class AbstractFactory
{

    const
        CREATE  = 'POST',
        UPDATE  = 'PATCH',
        REPLACE = 'PUT'
    ;

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function create(array $data);

    /**
     * @param array $data
     * @param $object
     * @return mixed
     */
    abstract public function replace(array $data, $object);

    /**
     * @param array $data
     * @param $object
     * @return mixed
     */
    abstract public function update(array $data, $object);
}