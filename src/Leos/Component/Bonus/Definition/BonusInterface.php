<?php
namespace Leos\Component\Bonus\Definition;


use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Wallet\Definition\CurrencyInterface;

interface BonusInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface;

    /**
     * @param UserInterface $user
     * @return BonusInterface
     */
    public function setUser(UserInterface $user): BonusInterface;

    /**
     * @return int
     */
    public function getPromo(): int;

    /**
     * @param int $promo
     * @return BonusInterface
     */
    public function setPromo(int $promo): BonusInterface;

    /**
     * @return int
     */
    public function getAmountReal(): int;

    /**
     * @param int $amountReal
     * @return BonusInterface
     */
    public function setAmountReal(int $amountReal): BonusInterface;

    /**
     * @return int
     */
    public function getAmountBonus(): int;

    /**
     * @param int $amountBonus
     * @return BonusInterface
     */
    public function setAmountBonus(int $amountBonus): BonusInterface;

    /**
     * @return CurrencyInterface
     */
    public function getCurrency(): CurrencyInterface;

    /**
     * @param CurrencyInterface $currency
     * @return BonusInterface
     */
    public function setCurrency(CurrencyInterface $currency): BonusInterface;
}
