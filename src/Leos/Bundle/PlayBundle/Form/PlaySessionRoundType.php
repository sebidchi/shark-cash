<?php

namespace Leos\Bundle\PlayBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PlaySessionRoundType
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Form
 */
class PlaySessionRoundType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roundId')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Leos\Bundle\PlayBundle\Entity\PlaySessionRound',
            'csrf_protection' => false
        ));
    }
}
