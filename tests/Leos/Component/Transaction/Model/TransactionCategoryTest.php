<?php
namespace Tests\Leos\Component\Transaction\Model;

use Leos\Component\Transaction\Model\TransactionCategory;

/**
 * Class TransactionCategoryTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Tests\Leos\Component\Transaction\Model
 */
class TransactionCategoryTest extends \PHPUnit_Framework_TestCase
{

    public function testTransactionCategory()
    {
        $category = self::getTransactionCategory($name = "bet");

        $this->assertEquals($name, $category->getName());

        $this->assertEquals(TransactionCategory::DEBIT, $category->getType());

        $category->setName($newName = "win");
        $this->assertEquals($newName, $category->getName());

        $this->assertEquals(TransactionCategory::CREDIT, $category->getType());
        $this->assertNotNull($category->getId());
    }

    /**
     * @expectedException \LogicException
     */
    public function testTransactionCategoryKO()
    {
        $category = self::getTransactionCategory($name = "bet");

        $category->setName($newName = "notExist!");
    }

    /**
     * @param string $name
     * @return TransactionCategory
     */
    public static function getTransactionCategory(string $name): TransactionCategory
    {
        return new TransactionCategory($name);
    }
}