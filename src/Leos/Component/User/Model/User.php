<?php
namespace Leos\Component\User\Model;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Wallet\Definition\CurrencyInterface;

/**
 * Class User
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Model
 */
class User implements UserInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * @var string
     */
    protected $username = '';

    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @var string
     */
    protected $password = '';

    /**
     * @var string
     */
    protected $salt = '';

    /**
     * @var string[]
     */
    protected $roles = [];

    /**
     * @var CurrencyInterface
     */
    protected $currency;

    /**
     * User constructor.
     * @param string $username
     * @param array $roles
     */
    public function __construct(string $username = null, array $roles = [])
    {
        $this->createdAt = new \DateTime();
        $this->username = $username ?: '';
        $this->roles = $roles;
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserInterface
     */
    public function setUsername($username): UserInterface
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserInterface
     */
    public function setPassword(string $password): UserInterface
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return UserInterface
     */
    public function setRoles(array $roles): UserInterface
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param CurrencyInterface $currency
     * @return UserInterface
     */
    public function setCurrency($currency): UserInterface
    {
        $this->currency = $currency;

        return $this;
    }

}
