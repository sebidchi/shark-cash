<?php
namespace Leos\Component\User\Exception;

/**
 * Class UserAlreadyExistException
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Exception
 */
class UserAlreadyExistException extends \LogicException
{

    /**
     * UserAlreadyExistException constructor.
     */
    public function __construct()
    {
        parent::__construct("user.exception.username_already_exist", 409);
    }
}