<?php
namespace Leos\Component\Play\Factory;
use Leos\Component\Play\Model\PlaySessionRound;

/**
 * Class PlaySessionRoundFactoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Factory
 */
interface PlaySessionRoundFactoryInterface
{
    /**
     * @param array $data
     *
     * @return PlaySessionRound
     */
    public function create(array $data): PlaySessionRound;
}